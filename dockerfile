# multistage Dockerfile
ARG TAG=14.17.5
FROM node:$TAG as build
WORKDIR /app
COPY . .
RUN npm install

FROM node:lts-alpine as final
WORKDIR /app
COPY --from=build /app .
EXPOSE 3000
CMD [ "npm", "start" ]
